///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Patrick Manuel <pamanuel@hawaii.edu>
// @date   _16_02_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>

int main() {

    struct tm bday;
    bday.tm_year = (2022 - 1900);
    bday.tm_mon  = 2;
    bday.tm_mday = 15;
    bday.tm_sec  = 0;         /* seconds,  range 0 to 59          */
    bday.tm_min  = 0;         /* minutes, range 0 to 59           */
    bday.tm_hour = 0;       /* hours, range 0 to 23             */

    time_t seconds = mktime(&bday);

    printf("Reference time: %s", asctime(&bday));

    int year;
    int day;
    int hour;
    int minute;
    int sec;
    time_t currenttime = time(0);
    time_t timeleft = difftime(seconds, currenttime);

    while ( true ){    
    if ( timeleft == -1 ){
        printf("Time's Up"); 
        }
    year    = timeleft / 31536000;
    day     = (timeleft % 31536000) / 86400;
    hour    = (timeleft % 31536000 % 86400) / 3600 ;
    minute  = (timeleft % 31536000 % 86400 % 3600) / 60 ;
    sec = ((timeleft % 31536000 % 86400 % 3600) % 60);

    timeleft -= 1;
    sleep(1); 
    printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d \n", year, day, hour, minute, sec);
    }
}

